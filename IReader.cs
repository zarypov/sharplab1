﻿using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade
{
    /// <summary>
    /// Online library reader interface
    /// </summary>
    /// <typeparam name="T">Type of electronic book</typeparam>
    public interface IReader<in T> where T : IEbook
    {
        /// <summary>
        /// Opens a book
        /// </summary>
        /// <param name="book">The book to open</param>
        void Open(T book);
    }
}