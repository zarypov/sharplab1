﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheMindSharperThanAnyBlade.Things;
using TheMindSharperThanAnyBlade.Utilities;
using TheMindSharperThanAnyBlade.Utilities.Logger;

namespace TheMindSharperThanAnyBlade
{
    /// <summary>
    /// Library collection interface
    /// </summary>
    /// <typeparam name="T">Type of the things stored in collection</typeparam>
    public interface ILibrary<T> : ICollection<T>, ICloneable where T : IThing
    {
        /// <summary>
        /// Library indexator
        /// </summary>
        /// <param name="index">Index</param>
        /// <returns>The element of the library with the Index</returns>
        T this[int index] { get; set; }
        /// <summary>
        /// Library information
        /// </summary>
        /// <returns>Information about books stored in library</returns>
        string GetInfo();
    }

    class Library<T> : ILibrary<T> where T : IThing
    {
        /// <summary>
        /// A list of books
        /// </summary>
        private List<T> books = new List<T>();

        public delegate int ThingComparator<T> (T a, T b);
        public delegate void MySuperSort<T> (ThingComparator<T> a);
        /// <summary>
        /// Library logger
        /// </summary>
        Logger<Library<T>> logger = new Logger<Library<T>>();

        public void CombSort(ThingComparator<T> comp)
        {
            double factor = 1.247;
            int step = books.Count - 1;
            bool swap = true;
            while (step > 1)
            {
                swap = false;
                for (int i = 0; i + step < books.Count; i++)
                {
                    if (comp(books[i], books[i + step]) == 1)
                    {
                        T bom = books[i];
                        books[i] = books[i + step];
                        books[i + step] = bom;
                        swap = true;
                    }
                }
                step = (int)(step / factor);
            }
            for (int j = 0; j < books.Count; j++)
            {
                swap = false;
                for (int k = 0; k < books.Count - j - 1; k++)
                {
                    if (comp(books[k], books[k + 1]) == 1)
                    {
                        T bom = books[k];
                        books[k] = books[k + 1];
                        books[k + 1] = bom;
                        swap = true;
                    }
                }
                if (!swap) break;
            }
        }
        
        public Library()
        {
        }

        public Library(IEnumerable<T> books)
        {
            this.books = books.ToList();//создает полную копию books
        }

        /// <summary>
        /// Universal enumerator getter
        /// </summary>
        /// <returns>Universal library enumerator</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return new BookEnumerator<T>(this);
        }
        /// <summary>
        /// Not-universal enumerator getter
        /// </summary>
        /// <returns>Non-universal library enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        /// <summary>
        /// Add an item in a library
        /// </summary>
        /// <param name="item">Item</param>
        public void Add(T item)
        {
            books.Add(item);
            string st = String.Format("Book {0} was added on {1}", item.Title, DateTime.Now.ToString());
            logger.Log(st);
        }
        /// <summary>
        /// Clears all elements in library
        /// </summary>
        public void Clear()
        {
            books.Clear();
        }
        /// <summary>
        /// Contain checker
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>True if item is in library, false otherwise</returns>
        public bool Contains(T item)
        {
            return books.Contains(item);
        }
        /// <summary>
        /// Copy a list of books to array
        /// </summary>
        /// <param name="array">Array (where TO copy)</param>
        /// <param name="arrayIndex">Starting index of Array</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            books.CopyTo(array, arrayIndex);
        }
        /// <summary>
        /// Remove an item from library
        /// </summary>
        /// <param name="item">Item</param>
        /// <returns>True if item was in library, false otherwise</returns>
        public bool Remove(T item)
        {
            return books.Remove(item);
        }
        public T this[int index]
        {
            get { return books[index]; }
            set { books[index] = value; }
        }
        /// <summary>
        /// Count of books in library
        /// </summary>
        public int Count { get { return books.Count(); } }
        /// <summary>
        /// Library read-only marker
        /// </summary>
        public bool IsReadOnly { get; protected set; }
        /// <summary>
        /// Clone a library
        /// </summary>
        /// <returns>New copy of a library</returns>
        public object Clone()
        {
            return new Library<T>(books);
        }

        public string GetInfo()
        {
            var builder = new StringBuilder("Now in library:\n");
            foreach (var book in books)
            {
                string s = String.Format("{0} - \"{1}\"\n", book.Author, book.Title);
                builder.Append(s);
            }
            return builder.ToString();
        }
        /// <summary>
        /// Library sort
        /// </summary>
        /// <param name="sorter">Comparator</param>
        public void Sort(ThingComparator<T> sorter)
        {
            CombSort(sorter);
        }
        /// <summary>
        /// Do action with all items in library
        /// </summary>
        /// <param name="action">Action to do</param>
        public void DoAction(Action<T> action)
        {
            foreach(T book in books)
            {
                action(book);
            }
        }
        /// <summary>
        /// Example func delegate to compare string with library item title
        /// </summary>
        Func<T, string, int> func = (a, b) => a.Title.CompareTo(b);

        /// <summary>
        /// Do func with all items in library
        /// </summary>
        /// <param name="st">String to find among titles</param>
        public void DoFunc(string st)
        {
           bool marker = true;
           foreach (var book in books)
           {
               if (func(book, st) == 0) 
               {
                   Console.WriteLine("Found the title!");
                   marker = false;
               }
           }
           if (marker) Console.WriteLine("Not found! Bombom");
        }
    }
}
