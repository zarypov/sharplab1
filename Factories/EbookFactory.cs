﻿using System;

namespace TheMindSharperThanAnyBlade.Factories
{
    class EbookFactory<T> : IEbookFactory<T> where T : IEbook
    {
        public T CreateBook()
        {
            return (T) Activator.CreateInstance(typeof(T), "Idiot", "Dostoyevskii", 1.2,195);
        }
    }
}
