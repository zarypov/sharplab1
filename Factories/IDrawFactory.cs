﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade.Factories
{
    /// <summary>
    /// Drawing factory
    /// </summary>
    interface IDrawFactory
    {
        /// <summary>
        /// Self-explanatory
        /// </summary>
        /// <returns>Abstract Drawing</returns>
        IThing CreateAbstractDrawing();
    }
}
