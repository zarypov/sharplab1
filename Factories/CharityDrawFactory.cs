﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade.Factories
{
    class CharityDrawFactory : IDrawFactory
    {
        public IThing CreateAbstractDrawing()
        {
            string title, author;
            Console.WriteLine("Enter title and author of the drawing");
            title = Console.ReadLine();
            author = Console.ReadLine();
            const double curPercent = 50;
            string genre = "Abstract";
            var x = new CharityDrawing(title, author, genre, curPercent);
            return x;
        }
    }
}
