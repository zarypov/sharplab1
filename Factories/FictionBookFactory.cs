﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade.Factories
{
    class FictionBookFactory : IBookFactory
    {
        public IThing CreatePrintedBook()
        {
            Console.WriteLine("Enter title and author");
            string title, author, publisher = "Alejandro Zarypov";
            int publishYear = DateTime.Today.Year;
            title = Console.ReadLine();
            author = Console.ReadLine();
            var x = new PrintedBook(title, author, publisher, publishYear);
            return x;
        }
        public IThing CreateAudioBook()
        {
            Console.WriteLine("Enter name, author and length");
            string title, author, audioFormat = "mp3", length;
            title = Console.ReadLine();
            author = Console.ReadLine();
            length = Console.ReadLine();
            var x = new AudioBook(title, author, audioFormat, length);
            return x;
        }
        public IThing CreateEBook() 
        {
            Console.WriteLine("Enter name, author and size in MBs");
            string title, author, eFormat = "pdf";
            double size;
            title = Console.ReadLine();
            author = Console.ReadLine();
            size = Convert.ToDouble(Console.ReadLine());
            var x = new EBook(title, author, eFormat, size);
            return x;
        }
    }
}

