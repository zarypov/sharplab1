﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade.Factories
{
    class ScienceBookFactory : IBookFactory
    {
        public IThing CreatePrintedBook()
        {
            Console.WriteLine("Enter title");
            string title, author = "Alejandro Zarypov", publisher = "SUSU";
            int publishYear = DateTime.Today.Year;
            title = Console.ReadLine();
            var x = new PrintedBook(title, author, publisher, publishYear);
            return x;
        }
        public IThing CreateAudioBook()
        {
            Console.WriteLine("Enter name, author and length");
            string title, author, audioFormat = "wav", length;
            title = Console.ReadLine();
            author = Console.ReadLine();
            length = Console.ReadLine();
            var x = new AudioBook(title, author, audioFormat, length);
            return x;
        }
        public IThing CreateEBook() 
        {
            Console.WriteLine("Enter title and size in MBs");
            string title, author = "Alejandro Zarypov", eFormat = "docx";
            double size;
            title = Console.ReadLine();
            size = Convert.ToDouble(Console.ReadLine());
            var x = new EBook(title, author, eFormat, size);
            return x;
        }
    }
}
