﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade.Factories
{
    /// <summary>
    /// Abstract factory interface
    /// </summary>
    interface IBookFactory
    {
        /// <summary>
        /// Self-explanatory
        /// </summary>
        /// <returns>Printed book</returns>
        IThing CreatePrintedBook();
        /// <summary>
        /// Self-explanatory
        /// </summary>
        /// <returns>Audio book</returns>
        IThing CreateAudioBook();
        /// <summary>
        /// Self-explanatory
        /// </summary>
        /// <returns>Electronic book</returns>
        IThing CreateEBook();
    }
}
