﻿namespace TheMindSharperThanAnyBlade.Factories
{
    /// <summary>
    /// Electronic books factory
    /// </summary>
    /// <typeparam name="T">A type of electronic book</typeparam>
    public interface IEbookFactory<out T>  where T : IEbook
    {
        /// <summary>
        /// Self-explanatory
        /// </summary>
        /// <returns>Electronic book</returns>
        T CreateBook();
    }
}