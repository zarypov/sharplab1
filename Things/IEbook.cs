﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade
{
    /// <summary>
    /// Electronic book interface
    /// </summary>
    public interface IEbook : IThing
    {
        /// <summary>
        /// A format of electronic book
        /// </summary>
        string Format { get; set; }
        /// <summary>
        /// A size of electronic book (MB)
        /// </summary>
        double Size { get; set; }
    }
}