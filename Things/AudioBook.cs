﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Things
{
    public class AudioBook : ThingBase
    {
        public string Format { get; protected set; }
        public string Length { get; protected set; }
        public AudioBook(string title, string author, string format, string length)
            : base(title, author)
        {
            Format = format;
            Length = length;
        }
    }
}
