﻿using System.Collections;
using System.Collections.Generic;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade
{
    public class BookEnumerator<T> :IEnumerator<T> where T : IThing
    {
        private readonly ILibrary<T> library;
        private int curIndex;
        private T curItem;

        public BookEnumerator(ILibrary<T> library)
        {
            this.library = library;
            curItem = default(T);
            curIndex = -1;
        }

        public void Dispose()
        { }

        public bool MoveNext()
        {
            if (++curIndex >= library.Count)
            {
                return false;
            }

            curItem = library[curIndex];
            return true;
        }

        public void Reset()
        {
            curIndex = -1;
        }

        public T Current { get { return curItem; } }

        object IEnumerator.Current { get { return Current; } }
    }
}