﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Things
{
    public abstract class ThingBase : IThing
    {
        public string Title { get; protected set; }
        public string Author { get; protected set; }
        protected ThingBase (string title, string author)
        {
            Title = title;
            Author = author;
        }
    }
}
