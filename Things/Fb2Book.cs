﻿namespace TheMindSharperThanAnyBlade.Things
{
    public class Fb2Book : EBook
    {
        /// <summary>
        /// Page count of .fb2 book
        /// </summary>
        public int PagesCount { get; protected set; }

        public Fb2Book(string title, string author, double size, int pagesCount) : base(title, author, ".fb2", size)
        {
            PagesCount = pagesCount;
        }
    }
}