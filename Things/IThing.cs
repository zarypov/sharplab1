﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Things
{
    /// <summary>
    /// Publishing house thing interface
    /// </summary>
    public interface IThing
    {
        /// <summary>
        /// A title of a thing
        /// </summary>
        string Title { get; }
        /// <summary>
        /// An author of a thing
        /// </summary>
        string Author { get; }
    }
}
