﻿
namespace TheMindSharperThanAnyBlade.Things
{
    public class EpubBook : EBook
    {
        /// <summary>
        /// Page count of .epub book
        /// </summary>
        public int PagesCount { get; protected set; }

        public EpubBook(string title, string author, double size, int pagesCount) : base(title, author, ".epub", size)
        {
            PagesCount = pagesCount;
        }
    }
}