﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Things
{
    public class CharityDrawing : ThingBase
    {
        /// <summary>
        /// A genre of drawing
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Charity percentage 
        /// </summary>
        public double Percent { get; set; }
        public CharityDrawing(string title, string author, string genre, double percent)
            : base(title, author)
        {
            Genre = genre;
            Percent = percent;
        }
    }
}
