﻿namespace TheMindSharperThanAnyBlade.Things
{
    public class EBook : ThingBase, IEbook
    {
        public EBook(string title, string author, string format, double size)
            : base(title, author)
        {
            Format = format;
            Size = size;
        }
        public string Format { get; set; }
        public double Size { get; set; }
    }
}
