﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Things
{
    public class PrintedBook : ThingBase
    {
        /// <summary>
        /// A publisher of a printed book
        /// </summary>
        public string Publisher { get; protected set; }
        /// <summary>
        /// Year of publish
        /// </summary>
        public int PublishYear { get; protected set; }
        public PrintedBook(string title, string author, string publisher, int publishYear)
            : base(title, author)
        {
            Publisher = publisher;
            PublishYear = publishYear;
        }
    }
}
