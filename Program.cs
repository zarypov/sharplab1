﻿using System;
using TheMindSharperThanAnyBlade.Things;
using TheMindSharperThanAnyBlade.Factories;
using TheMindSharperThanAnyBlade.Utilities;
using TheMindSharperThanAnyBlade.Utilities.Logger;



namespace TheMindSharperThanAnyBlade
{
    class Program
    {
        
        //Action - open all books
        private static void OpenAll(IThing book)
        {
            Console.WriteLine("The book {0} was opened.", book.Title);
        }

        static void Main(string[] args)
        {
            /*var factoryScience = new ScienceBookFactory();
            var factoryFiction = new FictionBookFactory();
            var factoryDraws = new CharityDrawFactory();
            List<IThing> scienceList = new List<IThing>();
            var book1 = factoryScience.CreatePrintedBook();
            var book2 = factoryScience.CreateAudioBook();
            var book3 = factoryScience.CreateEBook();
            scienceList.Add(book1);
            scienceList.Add(book2);
            scienceList.Add(book3);
            book1 = factoryFiction.CreatePrintedBook();
            book2 = factoryFiction.CreateAudioBook();
            book3 = factoryFiction.CreateEBook();
            scienceList.Add(book1);
            scienceList.Add(book2);
            scienceList.Add(book3);
            book1 = factoryDraws.CreateAbstractDrawing();
            scienceList.Add(book1);
            Console.ReadKey();

            var reader = new Reader<EBook>();
            IReader<Fb2Book> r2 = reader; //контрвариантность
            
            var ebookFactory = new EbookFactory<Fb2Book>();
            IEbookFactory<EBook> factory = ebookFactory; //ковариантность
            var fb2Book = ebookFactory.CreateBook();*/

            Logger.OnLog += LogHandlers.ForConsole;

            var lib = new Library<PrintedBook>();
            
            lib.Add(new PrintedBook("War and Peace", "L.N. Tolstoy", "Piter", 2006));
            lib.Add(new PrintedBook("1981", "George Orwell", "Going Quantum", 1981));
            lib.Add(new PrintedBook("1985", "George Orwell", "Going Quantum", 1985));
            lib.Add(new PrintedBook("1987", "George Orwell", "Going Quantum", 1987));
            lib.Add(new PrintedBook("1982", "George Orwell", "Going Quantum", 1982));
            lib.Add(new PrintedBook("1984", "George Orwell", "Going Quantum", 1984));
            lib.Add(new PrintedBook("1989", "George Orwell", "Going Quantum", 1989));
            lib.Add(new PrintedBook("1999", "George Orwell", "Going Quantum", 1999));
            lib.Add(new PrintedBook("2012", "George Orwell", "Going Quantum", 2012));
            lib.Add(new PrintedBook("2005", "George Orwell", "Going Quantum", 2005));
            Console.WriteLine(lib.GetInfo());
            
            var libCopy = (Library<PrintedBook>)lib.Clone();
            libCopy.Add(new PrintedBook("12 small african-american boys in a hood", "Agata Christie", "Eksdii", 2003));
            //libCopy.Add(new PrintedBook("Hello", "Adele", "Rolling in the Deep", 2009));

            libCopy.Sort(Comparators.ByAuthor);
            lib.Sort(Comparators.ByPublishYear);

            lib.Sort(Comparators.ByPublishYear);
            Console.WriteLine(libCopy.GetInfo());
            libCopy.DoFunc("Hello");
            Console.WriteLine(lib.GetInfo());

            //lib.DoAction(OpenAll);
         
            Console.Read();
        }

    }
}
