﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Utilities.Logger
{
    /// <summary>
    /// Class, event arguments description
    /// </summary>
    public class LogEventArgs : EventArgs
    {
        /// <summary>
        /// Log message
        /// </summary>
        public string Message { get; protected set; }

        public LogEventArgs(string message)
        {
            Message = message;
        }
    }
}
