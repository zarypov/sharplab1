﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Utilities.Logger
{
    /// <summary>
    /// Class, log handlers description
    /// </summary>
    public static class LogHandlers
    {
        /// <summary>
        /// Console log handler
        /// </summary>
        /// <param name="args">Event arguments</param>
        public static void ForConsole(LogEventArgs args)
        {
            Console.WriteLine(args.Message);
        }
        /// <summary>
        /// File log handler
        /// </summary>
        /// <param name="args">Event arguments</param>
        public static void ForFile(LogEventArgs args)
        {
            var writer = new StreamWriter("prolog.txt", true);
            writer.WriteLine(args.Message);
            writer.Close();
        }
    }
}
