﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;


namespace TheMindSharperThanAnyBlade.Utilities.Logger
{
    /// <summary>
    /// Self-explanatory
    /// </summary>
    static class Logger
    {
        /// <summary>
        /// Event on logging
        /// </summary>
        public static event LogEventHandler OnLog;
        /// <summary>
        /// Function that calls OnLog
        /// </summary>
        /// <param name="args"></param>
        public static void CallOnLog(LogEventArgs args)
        {
            if (OnLog != null) OnLog.Invoke(args);
        }
    }
    /// <summary>
    /// Logger base class
    /// </summary>
    /// <typeparam name="T">Type of class that logs a message</typeparam>
    class Logger<T> : ILogger
    {
        public void Log(string message)
        {
            string st = String.Format("{0} : {1}", typeof(T).Name, message);
            var args = new LogEventArgs(st);
            Logger.CallOnLog(args);
        }
    }
}
