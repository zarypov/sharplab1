﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMindSharperThanAnyBlade.Utilities.Logger
{
    /// <summary>
    /// Function for event handling
    /// </summary>
    /// <param name="args">Event arguments</param>
    delegate void LogEventHandler(LogEventArgs args);
    /// <summary>
    /// Logger interface
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Function that logs a message
        /// </summary>
        /// <param name="message">A message</param>
        void Log(string message);
    }
}
