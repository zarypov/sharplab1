﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMindSharperThanAnyBlade.Things;

namespace TheMindSharperThanAnyBlade.Utilities
{
    class Comparators
    {
        /// <summary>
        /// Sort by author (in alphabetical order)
        /// </summary>
        /// <param name="t1">First element</param>
        /// <param name="t2">Second element</param>
        /// <returns>Standart Compare -1 0 1</returns>
        public static int ByAuthor(IThing t1, IThing t2)
        {
            return (String.Compare(t1.Author, t2.Author, true));
        }
        /// <summary>
        /// Sort by title (in alphabetical order)
        /// </summary>
        /// <param name="t1">First element</param>
        /// <param name="t2">Second element</param>
        /// <returns>Standart Compare -1 0 1</returns>
        public static int ByTitle(IThing t1, IThing t2)
        {
            return (String.Compare(t1.Title, t2.Title, true));
        }
        /// <summary>
        /// Sort ebooks by format (in alphabetical order)
        /// </summary>
        /// <param name="t1">First element</param>
        /// <param name="t2">Second element</param>
        /// <returns>Standart Compare -1 0 1</returns>
        public static int ByEBookFormat(IEbook t1, IEbook t2)
        {
            return (String.Compare(t1.Format, t2.Format, true));
        }
        /// <summary>
        /// Sort printed books by publish date (in ascending order)
        /// </summary>
        /// <param name="t1">First element</param>
        /// <param name="t2">Second element</param>
        /// <returns>Standart Compare -1 0 1</returns>
        public static int ByPublishYear(PrintedBook t1, PrintedBook t2)
        {
            if (t1.PublishYear < t2.PublishYear) return -1;
            else if (t1.PublishYear > t2.PublishYear) return 1;
            else return 0;
        }
    }
}
