﻿using System;

namespace TheMindSharperThanAnyBlade
{
    public class Reader<T> : IReader<T> where T : IEbook
    {
        public void Open(T book)
        {
            Console.WriteLine("The book {0} was opened. Size is {1} MB", book.Title, book.Size);
        }
    }
}